-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2021 at 04:52 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `databasenew`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(10) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `no_hp` int(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `nama`, `username`, `password`, `no_hp`) VALUES
(1, 'admin', 'admin1', '21232f297a57a5a743894a0e4a801fc3', 67762827);

-- --------------------------------------------------------

--
-- Table structure for table `catat`
--

CREATE TABLE `catat` (
  `nama_pengguna` varchar(35) NOT NULL,
  `bulan` varchar(12) NOT NULL,
  `jumlah` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `catat`
--

INSERT INTO `catat` (`nama_pengguna`, `bulan`, `jumlah`) VALUES
('iswanto', 'agustus', 4200000);

-- --------------------------------------------------------

--
-- Table structure for table `laporan`
--

CREATE TABLE `laporan` (
  `id` int(50) NOT NULL,
  `id_pengguna` varchar(15) NOT NULL,
  `bulan_lalu` int(50) NOT NULL,
  `bulan_ini` int(50) NOT NULL,
  `m3` int(50) NOT NULL,
  `jumlah_tagihan` int(50) NOT NULL,
  `admin` int(50) NOT NULL,
  `total` int(50) NOT NULL,
  `ket` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `laporan`
--

INSERT INTO `laporan` (`id`, `id_pengguna`, `bulan_lalu`, `bulan_ini`, `m3`, `jumlah_tagihan`, `admin`, `total`, `ket`) VALUES
(1, 'iswanto', 298, 314, 16, 40000, 2000, 42000, '');

-- --------------------------------------------------------

--
-- Table structure for table `pembukuan`
--

CREATE TABLE `pembukuan` (
  `id` int(10) NOT NULL,
  `id_pengguna` int(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(20) NOT NULL,
  `telepon` int(20) NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'aktif',
  `id_petugas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembukuan`
--

INSERT INTO `pembukuan` (`id`, `id_pengguna`, `nama`, `alamat`, `telepon`, `status`, `id_petugas`) VALUES
(11111, 2147483647, 'iswanto', 'dusun_selanyah', 821346647, 'aktif', 0),
(11116, 2, 'Sutrisno', 'dusun selanyah', 2147483647, 'aktif', 12),
(11117, 2, 'John Doe', 'Jakarta', 2147483647, 'aktif', 1234),
(11118, 1002, 'Jane Doe', 'NY', 854, 'aktif', 1234);

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int(10) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `username` varchar(35) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `nama`, `username`, `password`) VALUES
(12, 'petugas', 'petugas', 'afb91ef692fd08c445e8cb1bab2ccf9c'),
(1234, 'petugas', 'petugas1', 'afb91ef692fd08c445e8cb1bab2ccf9c');

-- --------------------------------------------------------

--
-- Table structure for table `tagihan`
--

CREATE TABLE `tagihan` (
  `no` int(20) NOT NULL,
  `nama` varchar(35) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `bulan_lalu` int(10) NOT NULL,
  `bulan_ini` int(10) NOT NULL,
  `meter_kubik` int(10) NOT NULL,
  `jumlah_tagihan` int(10) NOT NULL,
  `admin` int(10) NOT NULL,
  `total` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tagihan`
--

INSERT INTO `tagihan` (`no`, `nama`, `alamat`, `bulan_lalu`, `bulan_ini`, `meter_kubik`, `jumlah_tagihan`, `admin`, `total`) VALUES
(1, 'iswanto', 'dusun_selanyah', 298, 314, 9, 40000, 2000, 42000);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_pengguna` int(20) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `meter_bulan_lalu` int(10) NOT NULL,
  `meter_bulan_ini` int(10) NOT NULL,
  `jumlah_meter` int(10) NOT NULL,
  `tagihan` int(10) NOT NULL,
  `admin` int(10) NOT NULL,
  `total` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_pengguna`, `alamat`, `meter_bulan_lalu`, `meter_bulan_ini`, `jumlah_meter`, `tagihan`, `admin`, `total`) VALUES
(1, '', 0, 0, 0, 0, 0, 0),
(2, '', 0, 0, 0, 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `laporan`
--
ALTER TABLE `laporan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembukuan`
--
ALTER TABLE `pembukuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- Indexes for table `tagihan`
--
ALTER TABLE `tagihan`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_pengguna`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `laporan`
--
ALTER TABLE `laporan`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pembukuan`
--
ALTER TABLE `pembukuan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11119;

--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_petugas` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1235;

--
-- AUTO_INCREMENT for table `tagihan`
--
ALTER TABLE `tagihan`
  MODIFY `no` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_pengguna` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
