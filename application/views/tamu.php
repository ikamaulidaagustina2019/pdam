<!DOCTYPE html>
<html>
<head>
  <title></title>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
   <link rel="stylesheet" href="https://kit-pro.fontawesome.com/releases/v5.12.1/css/pro.min.css">
</head>
<body>
<div class="container">
  <hr/>
  <div class="card" style="width:400px;margin: auto;box-shadow: 0 0 5px rgba(0,0,0,0.4)">
    <div class="card-body">
       <img src="<?=base_url('assets/img/logo.jpg')?>"style="width:10%">
      <strong class="capitalize ml-1 flex-1">PAMSIMAS</strong>
      <br><br>
      <a href="login" class="btn btn-outline-primary" style="width: 100%">
        <i class="fas fa-user-tie"></i> Admin
      </a>
      <br><br>
      <a href="login" class="btn btn-outline-success" style="width: 100%">
        <i class="fas fa-user-friends"></i> Petugas
      </a>
      <br><br>
      <a href="users" class="btn btn-outline-secondary" style="width: 100%">
        <i class="fas fa-users"></i>  Users
      </a>
      <br><br>
      
      
    </div>
  </div>
  <br>
</div>
</body>
</html>