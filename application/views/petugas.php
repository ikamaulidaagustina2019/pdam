<!DOCTYPE html>
<html>
<head>
  <title></title>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
   <link rel="stylesheet" href="https://kit-pro.fontawesome.com/releases/v5.12.1/css/pro.min.css">
</head>
<body>
<div class="container">
  <hr/>
  <div class="card" style="width:600px;margin: auto;box-shadow: 0 0 5px rgba(0,0,0,0.4)">
    <div class="card-header">
      <strong class="capitalize ml-1 flex-1">Catat Meter</strong>
    </div>
    <div class="card-body">
      <form action="<?php echo base_url('petugas/catat');?>" method="post">
      <label>Bulan</label>
      <select class="form-control" name="bulan" style="width: 105px;">
        <option value="Januari">Januari</option>
        <option value="Februari">Februari</option>
        <option value="Maret">Maret</option>
        <option value="April">April</option>
        <option value="Mei">Mei</option>
        <option value="Juni">Juni</option>
        <option value="Juli">Juli</option>
        <option value="Agustus">Agustus</option>
        <option value="September">September</option>
        <option value="Oktober">Oktober</option>
        <option value="November">November</option>
        <option value="Desember">Desember</option>
      </select>
      <br><br>
      <table class="table-auto w-full text-left">
        <thead>
          
          
            <tbody class="text-gray-600">
             <tr>
            <td class="border border-l-0 px-4 py-2 text-center text-green-500"><b>No</b></td>                 
            <td class="border border-l-0 px-4 py-2 text-center text-green-500"><b>Nama Pengguna</b></td>
            <td class="border border-l-0 px-4 py-2 text-center text-green-500" ><b>Meter Kubik</b></td>
          </tr>

               <?php $no=1; foreach ($petugas as $u): ?>

                 <tr>
                   <td><?= $no; $no++; ?></td>
                   <td><?= $u->nama_pengguna?></td>
                   <td><input type="text" class="form-control" name="<?= $u->id_catat?>"></td>
                 </tr>
               <?php endforeach?>

            </tbody>
         
          </form>
        </thead>
           

      </table>
      <br><br>
        
       <button class='btn bg-purple-700 text-gray-100' style= "background: #4287f5">Submit</button>
      
    </div>
    
  </div>
  <br>
</div>
</body>
</html>
