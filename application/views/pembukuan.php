	<!-- strat content -->
	<div class="bg-gray-100 flex-1 p-6 md:mt-16">	 
		<!-- General Report -->

			<!-- <button class=""	data-bs-toggle="modal" data-bs-target="#popup"><i class="fas fa-plus"></i></button> -->

			<!-- modal -->
			<a href="<?= base_url('pembukuan/tampilTambah') ?>" class="btn bg-green-700 text-gray-100" style="width: 70px;"><i class="fas fa-plus"></i></a>
			
		
		<div class="grid">

					
			<div class="card">
				<div class="card-header" style="background: #a9eafc"><b>Pembukuan</b>
						 
				</div>
				
		<div class="card-body">
				<table class="table table-hover">
						<tr>
								<th>ID Pengguna</th>
								<th>Nama</th>
								<th>Alamat</th>
								<th>Telepon</th>
								<th>Status</th>
								<th>Aksi</th>
						</tr>
						<?php foreach ($pembukuan as $p): ?>
								
						
						<tr>
								<td><?= $p->id_pengguna ?></td>
								<td><?= $p->nama ?></td>
								<td><?= $p->alamat ?></td>
								<td><?= $p->telepon ?></td>
								<td><?= $p->status ?></td>
								<td class="inline-flex">
										<a href="<?= base_url('pembukuan/edit?id='.$p->id) ?>" class="btn bg-yellow-700 text-gray-100"><i class="fas fa-edit"></i></a>
										<a href="<?= base_url('pembukuan/hapus?id='.$p->id) ?>" class="btn bg-red-700 text-gray-100"><i class="fas fa-trash"></i></a>

								</td>
						</tr>
						<?php endforeach ?>
				</table>
		</div>

</div> 
	</div>
	<!-- end content -->


</div>

<script>
	var openmodal = document.querySelectorAll('.modal-open')
	for (var i = 0; i < openmodal.length; i++) {
		openmodal[i].addEventListener('click', function(event){
		event.preventDefault()
		toggleModal()
		})
	}
	
	const overlay = document.querySelector('.modal-overlay')
	overlay.addEventListener('click', toggleModal)
	
	var closemodal = document.querySelectorAll('.modal-close')
	for (var i = 0; i < closemodal.length; i++) {
		closemodal[i].addEventListener('click', toggleModal)
	}
	
	document.onkeydown = function(evt) {
		evt = evt || window.event
		var isEscape = false
		if ("key" in evt) {
		isEscape = (evt.key === "Escape" || evt.key === "Esc")
		} else {
		isEscape = (evt.keyCode === 27)
		}
		if (isEscape && document.body.classList.contains('modal-active')) {
		toggleModal()
		}
	};
	
	
	function toggleModal () {
		const body = document.querySelector('body')
		const modal = document.querySelector('.modal')
		modal.classList.toggle('opacity-0')
		modal.classList.toggle('pointer-events-none')
		body.classList.toggle('modal-active')
	}
	
	 
</script>

