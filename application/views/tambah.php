<form action="<?= base_url().'pembukuan/tambahPembukuan' ?>" method="post">
		<table class="table table-borderless">
				<tr>
						<td>Id Pengguna</td>
						<td>
						<input class="py-2 px-3 rounded-lg border-2 border-purple-300 mt-1 focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" type="text" placeholder="Id Pengguna" name="id_pengguna"></td>
				</tr>
				<tr>
						<td>Nama</td>
						<td>
						<input class="py-2 px-3 rounded-lg border-2 border-purple-300 mt-1 focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" type="text" placeholder="Nama" name="nama"></td>
				</tr>
				<tr>
						<td>Alamat</td>
						<td>
						<input class="py-2 px-3 rounded-lg border-2 border-purple-300 mt-1 focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" type="text" placeholder="Alamat" name="alamat"></td>
				</tr>
				<tr>
						<td>No. Telepon</td>
						<td>
						<input class="py-2 px-3 rounded-lg border-2 border-purple-300 mt-1 focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" type="text" placeholder="Telepon" name="telepon"></td>
				</tr>
				<tr>
						<td>Id Petugas</td>
						<td>
						<input class="py-2 px-3 rounded-lg border-2 border-purple-300 mt-1 focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" type="text" placeholder="Id Petugas" name="id_petugas"></td>
				</tr>
		</table>
		<button class='btn bg-purple-700 text-gray-100'>Simpan</button>
</form>					