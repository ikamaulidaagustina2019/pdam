<?php

class Model extends CI_Model {
	function cek_login($table,$where) {
		return $this->db->get_where($table,$where);
	}

	function cek_submit($table,$where) {
		return $this->db->get_where($table,$where);
	}
	function tambah($table, $data) {
		$this->db->insert($table, $data);
	}

	function ambilData($table) {
		return $this->db->get_where($table)->result();
		$data = [
      'us' => $this->model->getKondisi('pembukuan', ['id_petugas' => $this->session->userdata('id')])
    ];
    $this->load->view('petugas', $data);
	}

	function edit($table,$data,$kondisi){
		$this->db->where($kondisi);
		$this->db->update($table, $data);
	}

	function hapus($table, $data){
		$this->db->delete($table, $data);
	}

	function satu($table, $kondisi) {
		return $this->db->get_where($table, $kondisi)->row();
	}

	function getKondisi($table, $kondisi) {
		return $this->db->get_where($table, $kondisi)->result();
	}
	function masukaan_Data($table, $data){
		return $this->db->get_where($table)->result();
		$data = [
      'us' => $this->model->getKondisi('petugas', ['nama' => $this->session->userdata('nama')])
    ];
    $this->load->view('petugas', $data);
	}

	function ambil_data($table, $data) {
		return $this->db->get_where($table)->result();
		$data = [
      'us' => $this->model->getKondisi('laporan', ['id_pengguna' => $this->session->userdata('id')])
    ];
    $this->load->view('laporan', $data);
    
	}

	function masuk_Data($table, $data) {
		return $this->db->get_where($table)->result();
		$data = [
      'us' => $this->model->getKondisi('tagihan', ['nama' => $this->session->userdata('nama')])
    ];
    $this->load->view('tagihan', $data);
    
	}

	function simpan($table, $data){
		$this->db->send($table, $data);
	}


	


}


