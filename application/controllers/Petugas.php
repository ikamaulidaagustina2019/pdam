<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Petugas extends CI_Controller {

	function __construct(){
		parent::__construct();
		
		if($this->session->userdata('status') != "login") {
			redirect(base_url("login"));
		}
		$this->load->model('model');
	}
	function index() {
		$data = [
			'petugas' => $this->model->getKondisi('laporan', ['id_petugas' =>$this->session->userdata('id')]),
		];
		
		$this->load->view('petugas', $data);
			
	}
	function ambil_Data(){
		$laporan =$this->model->ambil_Data();
		print_r($laporan);
	}

	function catat() {
		print_r($_POST);
		$m3 = $_POST['m3'];
		unset($_POST['m3']);
		foreach ($_POST as $key => $value) {
			$data = array (
				'm3' => $value
			);
			$this->db->where('id_catat', $key);
			$this->db->update('laporan', $data);
		}
		redirect('laporan');
	}

	

}