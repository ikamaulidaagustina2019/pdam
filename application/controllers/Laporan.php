<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
	    
	public function __construct(){
		parent::__construct();
		// if($this->session->userdata('status') != "login") {
		// 	redirect(base_url("login/admin"));
		// }
		$this->load->model('model');
	}

	function index() {
		$data = [
			'laporan' => $this->model->ambilData('laporan'),
		];
		$this->load->view('header', $data);
		$this->load->view('laporan');
		$this->load->view('footer');
		
		
	}
	function ambil_data(){
		$laporan =$this->model->ambil_data();
		print_r($laporan);
	}
	

}

