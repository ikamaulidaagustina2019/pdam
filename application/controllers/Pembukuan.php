<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembukuan extends CI_Controller {
	    
	public function __construct(){
		parent::__construct();
		// if($this->session->userdata('status') != "login") {
		// 	redirect(base_url("login/admin"));
		// }
		$this->load->model('model');
	}

	function index() {
		$data = [
			'pembukuan' => $this->model->ambilData('pembukuan'),
		];
		$this->load->view('header', $data);
		$this->load->view('pembukuan');
		$this->load->view('footer');
	}

	function tambahPembukuan() {
		$dataInput = [
			'id_pengguna' => $_POST['id_pengguna'],
			'alamat' => $_POST['alamat'],
			'telepon' => $_POST['telepon'],
			'nama' => $_POST['nama'],
			'id_petugas' => $_POST['id_petugas'],
		];
		$this->model->tambah('pembukuan', $dataInput);
		redirect(base_url('pembukuan'));
	}

	function hapus() {
		$id = $_GET['id'];
		$this->model->hapus('pembukuan', ['id' => $id]);
		redirect(base_url('pembukuan'));
	}

	function tampilTambah() {
		$this->load->view('header');
		$this->load->view('tambah');
		$this->load->view('footer');
	}

	function edit() {
		$id = $_GET['id'];
		$dt['p'] = $this->model->satu('pembukuan', ['id' => $id]);
		$this->load->view('header', $dt);
		$this->load->view('edit');
		$this->load->view('footer');
	}

	function editProses() {
		$dataInput = [
			'id_pengguna' => $_POST['id_pengguna'],
			'alamat' => $_POST['alamat'],
			'telepon' => $_POST['telepon'],
			'nama' => $_POST['nama'],
		];
		$this->model->edit('pembukuan', $dataInput, ['id' => $_POST['id']]);
		redirect(base_url('pembukuan'));
	}
}

