<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	

    function __construct(){
          parent:: __construct();
          $this->load->model('model');
    }
    function index(){
    	$this->load->view('login');
    }
    function aksi_login(){
         $username = $this->input->post('username');
         $password = $this->input->post('password');
         $where = array(
              'username' => $username,
              'password' => md5($password)
              );
         $cek = $this->model->cek_login("admin",$where)->result_array();
         
         if(count($cek) > 0) {
          $data_session = array(
            'nama' => $username,
            'status' => "login"
         ); 
           $this->session->set_userdata($data_session);
            redirect(base_url("admin"));
        }
         $cek = $this->model->cek_login("petugas",$where)->num_rows();
        if($cek > 0) {
          $id = $this->model->satu('petugas', ['username' => $username])->id_petugas;
          $data_session = array(
              'nama' => $username,
              'id' => $id,
              'status' => "login"
           );
            $this->session->set_userdata($data_session);
            redirect(base_url("petugas"));
          }
         

          else {
              echo "Username dan password salah !";
        }            
}
function logout(){
$this->session->sess_destroy();
redirect(base_url('login'));
  }
}
        
 




