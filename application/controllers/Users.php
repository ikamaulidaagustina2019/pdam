<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	function __construct(){
		parent::__construct();

		if($this->session->userdata('status') != "submit") {
			redirect(base_url("submit"));
		}
		$this->load->model('model');
		
	}
		function index(){
			$data = [
			'users' => $this->model->getKondisi('users', ['id_pengguna' =>$this->session->userdata('id')]),
		];
		$this->load->view('users');
	}

}

	 
	

